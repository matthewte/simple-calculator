﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Calculator
{
    public struct Operation
    {
        double X { get; set; }
        double Y { get; set; }
        char Symbol { get; set; }
        double Result { get; set; }

        public double GetX() { return X; }
        public double GetY() { return Y; }
        public char GetSymbol() { return Symbol; }
        public double GetResult() { return Result; }
        public void SetResult(double r) { Result = r; }

        public Operation(double n1, double n2, char s)
        {
            X = n1;
            Y = n2;
            Symbol = s;
            Result = 0;
        }
    }

    class Program
    {
        //список элементов типа double, где будет храниться набор последних операций
        static List<Operation> memory = new List<Operation>();

        static void Main()
        {
            //изменяем кодировку в консоли для вывода кириллицы
            Console.OutputEncoding = Encoding.GetEncoding(1251);

            //переменные для хранения двух чисел для операции
            Operation op;

            //переменная для выхода из цикла
            bool repeatCycle = true;

            //переменная для выхода из приложения
            bool quit;

            //создаем цикл для непрерывной работы с калькулятором
            while (repeatCycle)
            {
                quit = true;
                //пытаемся выполнить подсчеты и ловим ошибки на ходу
                try
                {
                    //спрашиваем первое число
                    Console.WriteLine("Введите первое число, символ операции и второе число через Enter");
                    double x = GetNumber();
                    char s = GetOperation();
                    double y = GetNumber();
                    op = new Operation(x, y, s);

                    //подсчет в зависимости от операции
                    op.SetResult(Calculate(op));

                    //запись в список результатов новый результат
                    AddToMemory(memory, op);

                    //вывод текущего ответа и прошлых результатов
                    Console.WriteLine("Ответ: " + op.GetResult());
                    Console.Write("Прошлые результаты:" + ShowMemory(memory));
                    Console.WriteLine();
                }
                //если код операции не найден, то поймать ошибку
                catch (ArgumentOutOfRangeException ex)
                {
                    quit = false;
                    Console.WriteLine("ERROR 001: Операция не обнаружена, введите заново.");
                }
                //если при подсчетах с индексом в массиве ошибка, то поймать
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine("ERROR 002: " + ex.Message);
                }
                catch (InvalidCastException ex)
                {
                    quit = false;
                    Console.WriteLine("ERROR 003: Не удалось обработать число или операцию, введите заново.");
                }
                catch (FormatException ex)
                {
                    quit = false;
                    Console.WriteLine("ERROR 004: Некорректный ввод, введите заново.");
                }
                //если какая-то другая ошибка, то поймать
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR CMN: " + ex.Message);
                }
                finally
                {
                    //спрашиваем пользователя, хочет ли он выйти из приложения
                    while (quit)
                    {
                        Console.WriteLine("Продолжить? д/н");
                        try
                        {
                            switch (char.Parse(Console.ReadLine()))
                            {
                                case 'д':
                                    quit = false;
                                    break;
                                case 'н':
                                    quit = false;
                                    repeatCycle = false;
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (InvalidCastException ex)
                        {

                        }
                        catch (FormatException ex)
                        {

                        }
                    }
                }
            }
        }

        //функция для узнавания числа у пользователя
        public static double GetNumber()
        {
            double number = double.Parse(Console.ReadLine());
            return number;
        }

        //функция для узнавания операции у пользователя
        public static char GetOperation()
        {
            char symbol = char.Parse(Console.ReadLine());
            return symbol;
        }

        //функция для подсчета
        public static double Calculate(Operation op)
        {
            double result;

            switch (op.GetSymbol())
            {
                case '+':
                    {
                        result = op.GetX()+ op.GetY();
                        break;
                    }

                case '-':
                    {
                        result = op.GetX() - op.GetY();
                        break;
                    }

                case '*':
                    {
                        result = op.GetX() * op.GetY();
                        break;
                    }

                case '/':
                    {
                        result = op.GetX() / op.GetY();
                        break;
                    }

                case '^':
                    {
                        result = Math.Pow(op.GetX(), op.GetY());
                        break;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException();
                    }
            }
            return result;
        }

        //функция для сохранения в списке нового результата
        public static void AddToMemory(List<Operation> mem, Operation op)
        {
            if (mem.Count < 5)
            {
                mem.Add(op);
            }
            else
            {
                for (int n = 0; n < 4; n++)
                {
                    mem[n] = mem[n + 1];
                }
                mem[4] = op;
            }
        }

        //функция для вывода всех запомненных результатов в одну строку
        public static string ShowMemory(List<Operation> mem)
        {
            string memString = "\n";
            foreach (Operation op in mem)
            {
                memString += op.GetX() + " " + op.GetSymbol() + " " + op.GetY() + " = " + op.GetResult() + "\n";
            }

            return memString;
        }
    }
}